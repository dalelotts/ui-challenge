const jsonServer = require('json-server');
const path = require('path');
const update = require(path.dirname(__filename) + '/update');
const server = jsonServer.create();
const router = jsonServer.router(path.dirname(__filename) + '/data.json');
const middlewares = jsonServer.defaults();

/**
 * Update results then serve
 */
update.then((res) => {
  console.log(res);
  serve();
}, (err) => {
  console.log(err);
});

/**
 * Serve the data.json with json-server
 */
const serve = () => {
  server.use(middlewares);
  server.use(router);
  server.listen(8675, () => {
    console.log('World Cup Data is Served at http://localhost:8675');
  });
};
