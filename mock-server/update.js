const https = require('https');
const path = require('path');
const fs = require('fs');

/**
 * Get updated results from github, then write to the json file.
 * This would ideally be split out into functions and check if there are updates, but currently quick and dirty
 * @type {Promise<string>}
 */
module.exports = new Promise((resolve, reject) => {
  https.get('https://raw.githubusercontent.com/lsv/fifa-worldcup-2018/master/data.json', (res) => {
    const {statusCode} = res;
    let error;
    if (statusCode !== 200) {
      error = new Error('Request Failed.\n' + `Status Code: ${statusCode}`);
      reject(error);
    }
    if (error) {
      reject(error);
      res.resume();
      return;
    }
    res.setEncoding('utf8');
    let rawData = '';
    res.on('data', (chunk) => {
      rawData += chunk;
    });
    res.on('end', () => {
      try {
        const parsedData = JSON.parse(rawData);
        const file = path.dirname(__filename) + '/data.json';
        const content = JSON.stringify(parsedData, null, 2);
        fs.writeFile(file, content, (err) => {
          if (err) return console.log(err);
          resolve('World Cup Data Updated Successfully!');
        });
      } catch (e) {
        reject(e.message);
      }
    });
  }).on('error', (e) => {
    reject(e.message);
  });
});
