SnagApple UI Coding Challenge
============================

### Objective

The objective of this coding challenge is to consume the mock api to create a results view for the 2018 World Cup. A good example of this the group results page from fifa: https://www.fifa.com/worldcup/groups/. The format and appearance of the results page is entirely up to you.

### Tech Stack

You can use any framework of your choice or even vanilla javascript. The mock server should be kept in the repo and functioning. SnagApple primarily uses Angular 6, TypeScript and SCSS, so extra points for using that stack.

### Mock Server
The mock server uses [json-server](https://github.com/typicode/json-server) and a static JSON file to serve 2018 World Cup data in a REST api. The api is served with the `npm run server` command and runs at `http://localhost:8675`. Json-server provides a full restful api on the following resources:

* *stadiums*: Stadiums at which World Cup games are played
* *tvchannels*: TV channels at which World Cup games are shown across the world
* *teams*: Teams competing the World Cup games
* *groups*: Groups competing in the World Cup games; includes match results in the group stage.
* *knockout*: Knockout rounds in the World Cup; includes match results in the knockout stage.

The static file is updated from [GitHub - lsv/fifa-worldcup-2018: FIFA Worldcup 2018 - json data](https://github.com/lsv/fifa-worldcup-2018) when the server starts to update results.

For any information on using the mock api, see the [json-server readme](https://github.com/typicode/json-server) for information.

_Note: Please note groups and knockout are returned as an object rather than an array of objects._

### Questions

If you have any questions or concerns, please contact CJ at cj@mosure.org.